<?php

require_once "html.php";
$doc = new html("Aminoacid", array(
 "css" => "slog.css"
));

?>
<h1>Aminoacid</h1>
<p>Welcome to the Aminoacid app. This app lets you search trough the amino-acid structure of a protein of your choice. The data is sourced from scientific databases, based on genetic information that encodes this structure, though the information is intended to be used for nutritional purposes and similar.</p>
