<?php

include "begin.php";

?>
<p>For instance, the proteins <a href="profile.php?m=P35749&text=Myosin-11%20from%20Human">Myosin (and Actin)</a> represent most of the 20% protein makeup of muscle. Everybody knows one needs to eat protein to gain muscle mass, but proteins differ by the building blocks they are made of. Do beans trully contain the same building blocks of protein, that you require to build muscle mass?  Search below to find out. You may need to <a href="https://www.google.com/search?q=amino+acid+profile&tbm=isch" target="_blank">google "amino acid profile"</a> to get the composition of a vegetable that is not clearly classified by protein content.</p>

<h2>Search</h2>
<p>Enter what you are looking for. Because you are searching trough a bioinformatics protein database, note that trivial queries may not produce useful results. For example searching for "Myosin" is too general, you need to specify the exact protein variant, "Myosin-11" would work.</p>

<form method="GET" action="search.php">
 <input type="text" name="q" value="<?php if(isset($_GET['q'])) { echo $_GET['q']; } ?>">
 <input type="submit" name="s" value="Search" class="btn btn-primary">
</form>
