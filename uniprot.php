<?php

class uniprot {

 public $amino;

 function __construct() {
  $this->amino = array();
  $this->amino["A"] = array("Alanine", "Ala");
  $this->amino["R"] = array("Arginine", "Arg");
  $this->amino["N"] = array("Asparagine", "Asn");
  $this->amino["D"] = array("Aspartic acid", "Asp");
  $this->amino["C"] = array("Cysteine", "Cys");
  $this->amino["E"] = array("Glutamic acid", "Glu");
  $this->amino["Q"] = array("Glutamine", "Gln");
  $this->amino["G"] = array("Glycine", "Gly");
  $this->amino["H"] = array("Histidine", "His");
  $this->amino["I"] = array("Isoleucine", "Ile");
  $this->amino["L"] = array("Leucine", "Leu");
  $this->amino["K"] = array("Lysine", "Lys");
  $this->amino["M"] = array("Methionine", "Met");
  $this->amino["F"] = array("Phenylalanine", "Phe");
  $this->amino["P"] = array("Proline", "Pro");
  $this->amino["O"] = array("Pyrrolysine", "Pyl");
  $this->amino["U"] = array("Selenocysteine", "Sec");
  $this->amino["S"] = array("Serine", "Ser");
  $this->amino["T"] = array("Threonine", "Thr");
  $this->amino["W"] = array("Tryptophan", "Trp");
  $this->amino["Y"] = array("Tyrosine", "Tyr");
  $this->amino["V"] = array("Valine", "Val"); 
  $this->amino["X"] = array("unknown", "???"); 
 }

 function search($query = False) {
  if($query === False) {
   $query = $_GET["q"];
  }
  
  $xml = simplexml_load_file("http://www.uniprot.org/uniprot/?query=".rawurlencode($query)."&limit=10&format=xml");
  
  $out = array();
  foreach($xml->entry as $entry) {
   $id = (string) $entry->accession[0];
   $out[$id]["name"] = (string) $entry->protein->recommendedName->fullName[0];
      
   foreach($entry->organism->name as $name) {
   
    $found = False;
    if($name['type'] == "common") {
     $out[$id]["species"] = (string) $name;
     $found = True;
    }
    
    if(!$found) {
     $out[$id]["species"] = (string) $entry->organism->name;
    }
    
   }
  }
  
  return $out;
 
 }
 
 function sequence($m = False) {
  if($m === False) {
   $m = $_GET["m"];
  }
 
  $sequence = "";
  foreach(explode("\n", file_get_contents("http://www.uniprot.org/uniprot/".rawurlencode($m).".fasta")) as $line) {
   $firstChar = substr($line, 0, 1);
   if($firstChar != ">" && $firstChar != ";") {
    $sequence .= trim($line);
   }
  }
  
  return $sequence;
  
 }
 
 function profile($sequence = "", $proportonal = True) {
 
  $seqlen = strlen($sequence);

  //INIT PROFILE
  $data = array();
  foreach($this->amino as $code => $dummy) {
   $data[$code] = 0;
  }
  
  //COUNT AMINOS
  for ($i=0; $i<$seqlen; $i++) {
   $data[$sequence[$i]]++;
  }
  
  if($proportonal) {
   //DIVIDE BY TOTAL PROTEIN LENGTH
   foreach($this->amino as $code => $dummy) {
    $data[$code] = (float) $data[$code] / $seqlen;
   }
  }
  
  //OUTPUT
  foreach($this->amino as $code => $names) {
   $out["long"][$names[0]] = $data[$code];
   $out["short"][$names[1]] = $data[$code];
  }
    
  return $out;
  
 }

}

?>
