<?php

class chart {
 
 function js() {
  return "https://www.gstatic.com/charts/loader.js";
 }
 
 function code($data = array(), $options = array()) {

  if(isset($options["title"])) {
   $title = $options["title"];
  } else {
   $title = "";
  }

  if(isset($options["width"])) {
   $width = $options["width"];
  } else {
   $width = 1000;
  }

  if(isset($options["height"])) {
   $height = $options["height"];
  } else {
   $height = 600;
  }
  
  $tmp = array();
  foreach($data as $property => $value) {
   $tmp[] = array($property, $value);
  }
  $rows = json_encode($tmp);
 
 ?>
 
  <div id="chart_div"></div>
  <script type="text/javascript">

    // Load the Visualization API and the corechart package.
    google.charts.load('current', {'packages':['corechart']});

    // Set a callback to run when the Google Visualization API is loaded.
    google.charts.setOnLoadCallback(drawChart);

    // Callback that creates and populates a data table,
    // instantiates the pie chart, passes in the data and
    // draws it.
    function drawChart() {

      // Create the data table.
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Aminoacid');
      data.addColumn('number', 'Occurrences');
      data.addRows(<?php echo $rows; ?>);

      // Set chart options
      var options = {'title':'<?php echo $title; ?>',
                     'width':<?php echo $width; ?>,
                     'height':<?php echo $height; ?>};

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
      chart.draw(data, options);
    }
  </script> 
 
 <?php
 } 
 
}
