<?php

include "begin.php";

require_once "uniprot.php";
$u = new uniprot();

$molecules = $u->search($_GET["q"]);

?>
<h2>Select a protein</h2>
<p>Your search produced several results. The top 10 results are displayed below. Because you are searching trough a bioinformatics protein database, note that trivial queries may not produce useful results. For example searching for "Myosin" is too general, you need to specify the exact protein variant, "Myosin-11" would work. Please check the results before clicking a random protein.</p>
<p>Keep in mind that proteins are similar across species and you need not find the exact one.</p>

<ul>
<?php

foreach($molecules as $id => $m) {
 $text = $m["name"]." from ".$m["species"];
 ?><li><a href="profile.php?m=<?php echo rawurlencode($id); ?>&text=<?php echo rawurlencode($text); ?>"><?php echo $text; ?></li><?php
}

?>
</ul>
