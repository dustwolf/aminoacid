<?php

require_once "chart.php";
$c = new chart();

require_once "html.php";
$doc = new html("Aminoacid", array(
 "css" => "slog.css",
 "js" => $c->js()
));

?>
<h1>Aminoacid</h1>
<p>Welcome to the Aminoacid app. This app lets you search trough the amino-acid structure of a protein of your choice. The data is sourced from scientific databases, based on genetic information that encodes this structure, though the information is intended to be used for nutritional purposes and similar.</p>

<h2>Protein building blocks</h2>

<?php
require_once "uniprot.php";
$u = new uniprot();

$profile = $u->profile(
 $u->sequence($_GET["m"])
, False);

arsort($profile["long"]);

$c->code($profile["long"],array(
 "title" => $_GET["text"]
));

?>
